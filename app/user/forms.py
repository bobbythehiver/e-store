#-*- coding: UTF-8 -*-

from django.forms import ModelForm, PasswordInput, CharField
from .models import Image
from django.contrib.auth.models import User

class ImageForm(ModelForm):
    class Meta:
        model = Image
        fields = ('image_file', 'title', 'extra_cost')
        labels = {
            'image_file': u'Загрузите изображение',
            'title': u'Выберите название',
            'extra_cost': u'Установите наценку'
        }

class RegisterForm(ModelForm):
    class Meta:
        password = CharField(widget=PasswordInput)
        model = User
        fields = ('username', 'password', 'email')
        labels = {
            'username': u'Имя',
            'password': u'Пароль',
            'email': u'Электронный адрес'
        }
        widgets = {
            'password': PasswordInput(),
        }



class LoginForm(ModelForm):
    class Meta:
        password = CharField(widget=PasswordInput)
        model = User
        fields = ('username', 'password')
        labels = {
            'username': u'Имя',
            'password': u'Пароль'
        }
        widgets = {
            'password': PasswordInput(),
        }