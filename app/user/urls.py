from django.conf.urls import url

from . import views
import social.apps.django_app.urls
import paypal.standard.ipn.urls
from django.conf.urls import include, url


urlpatterns = [
    url(r'^$', views.main, name='main'),
    url(r'^user/catalog', views.catalog, name='catalog'),
    url(r'^user/shipping', views.shipping, name='shipping'),
    url(r'^user/preview', views.preview, name='preview'),
    url(r'^user/order', views.order, name='order'),
    url(r'^user/add-to-cart', views.add, name='add'),
    url(r'^user/construct-success', views.construct_success, name='construct_success'),
    url(r'^user/construct', views.construct, name='construct'),
    url(r'^user/save-order-options', views.save_options, name='save_options'),
    url(r'^user/delete-item', views.delete_item, name='delete_item'),
    url(r'^user/logout', views.account_logout, name='logout'),
    url(r'^user/login-user', views.login_user, name='login_user'),
    url(r'^user/register-user', views.register_user, name='register_user'),
    url(r'^accounts/profile', views.account_profile, name='profile'),
    
    url(r'^user/payment/cart', views.paypal_pay, name='cart'),
    url(r'^user/payment/success', views.paypal_success, name='payment_success'),
    url(r'^user/paypal/', include('paypal.standard.ipn.urls')),
]

