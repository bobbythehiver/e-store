from django.contrib import admin
from .models import Message, Image, Item, Order, Payment, Wages

admin.site.register(Message)
admin.site.register(Image)
admin.site.register(Item)
admin.site.register(Order)
admin.site.register(Payment)
admin.site.register(Wages)
