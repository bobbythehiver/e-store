#-*- coding: UTF-8 -*-

from __future__ import unicode_literals
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User

import random, string

class Message(models.Model):
    user = models.ForeignKey(User, default=None)
    text = models.CharField(max_length=100, default="")
    send_time = models.DateTimeField(default=timezone.now)


REJECT_REASONS = (
    (u'НАРУШЕНИЕ АВТОРСКОГО ПРАВА', 'copyright_violation'),
    ( u'ОСКОРБИТЕЛЬНЫЙ КОНТЕНТ', 'offensive_content'),
)

class Image(models.Model):
    image_file = models.ImageField(upload_to='', height_field=None, width_field=None, max_length=100, default=None)
    title = models.CharField(max_length=100, default="")
    author = models.ForeignKey(User, default=None)
    extra_cost = models.IntegerField(default=0)
    cost = models.IntegerField(default=0)
    upload_time = models.DateTimeField(default=timezone.now)
    accepted = models.BooleanField(default=False)
    reject_reason = models.CharField(max_length=50, choices=REJECT_REASONS, default='OK')

class Item(models.Model):
    image = models.ForeignKey(Image, default=None)
    color = models.CharField(max_length=10, default="")
    size = models.CharField(max_length=5, default="")
    cost = models.IntegerField(default=500)
    quantity = models.IntegerField(default=0)

def generate_token():
    token = [random.choice(string.ascii_letters + string.digits) for i in range(40)]
    return ''.join(token)

class Order(models.Model):
    user = models.ForeignKey(User, default=None, null=True, blank=True)
    item = models.ManyToManyField(Item, default=None)
    total_cost = models.IntegerField(default=0)
    total_quantity = models.IntegerField(default=0)
    create_time = models.DateTimeField(default=timezone.now)
    checkout_time = models.DateTimeField(default=timezone.now)
    shipping_region = models.CharField(max_length=40, default='')
    shipping_address = models.CharField(max_length=200, default='')
    token = models.CharField(max_length=40, default=generate_token)
    archive = models.BooleanField(default=False)

class Payment(models.Model):
    order = models.OneToOneField(Order, default=None)
    paypal_payment = models.IntegerField(default=0)
    create_time = models.DateTimeField(default=timezone.now)

class Wages(models.Model):
    user = models.ForeignKey(User, default=None)
    money = models.IntegerField(default=0)
    order = models.ForeignKey(Order, default=None)
    valid = models.BooleanField(default=False)