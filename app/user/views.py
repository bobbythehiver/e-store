#-*- coding: UTF-8 -*-

from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
from django.core.files.uploadedfile import SimpleUploadedFile
from paypal.standard.forms import PayPalPaymentsForm
from django.http import JsonResponse
from django.utils import timezone
from django.core.cache import cache
from django.http import Http404

from .models import Message, Image, Item, Order, Payment, Wages
from .forms import ImageForm, RegisterForm, LoginForm
from django.contrib.auth.models import User


def page404(request):
    return HttpResponseNotFound('<body style="background-color: #0E1014;"><h1 style="margin-top: 100px; margin-left:10%;color: white;"> Страница не найдена!</h1></body>')


def get_actual_messages(request):
    if request.user.is_authenticated():
        if cache.get('messages' + str(request.user.id)) is not None:
            return cache.get('messages' + str(request.user.id))
        try:
            images = Image.objects.filter(author=request.user)
            for image in images:
                message = ''
                if image.accepted:
                    message = u'Товар ' + str(image.id) + ' ' + image.title + ' добавлен в каталог'
                elif not image.reject_reason == 'OK':
                    message = u'Товар ' + str(image.id) + ' ' + image.title + ' отклонен. Причина: ' + image.reject_reason
                try:
                    if Message.objects.get(user=request.user, text=message) is None:
                        Message.objects.create(user=request.user, text=message)
                except Message.DoesNotExist:
                    Message.objects.create(user=request.user, text=message)
        except Image.DoesNotExist:
            pass

        try:
            cache.set('messages' + str(request.user.id), Message.objects.filter(user=request.user).order_by('send_time')[:5], 10)
            return cache.get('messages' + str(request.user.id))

        except Message.DoesNotExist:
            return None

    return None


def get_or_create_order(request):
    order = None
    if request.user is not None and request.user.is_authenticated():
        if 'token' in request.session:
            try:
                if Order.objects.filter(token=request.session['token'], archive=False) is not None:
                    order = Order.objects.filter(token=request.session['token'], archive=False).latest('create_time')
                    order.user = request.user
                    order.save()
                elif Order.objects.filter(user=request.user, archive=False) is not None:
                    order = Order.objects.filter(user=request.user, archive=False).latest('create_time')
                    request.session['token'] = order.token
                else:
                    order = Order.objects.create(user=request.user)
                    request.session['token'] = order.token
            except Order.DoesNotExist:
                try:
                    if Order.objects.filter(user=request.user, archive=False) is not None:
                        order = Order.objects.filter(user=request.user, archive=False).latest('create_time')
                        request.session['token'] = order.token
                    else:
                        order = Order.objects.create(user=request.user)
                        request.session['token'] = order.token
                except Order.DoesNotExist:
                    order = Order.objects.create(user=request.user)
        else:
            order = Order.objects.create(user=None)
            request.session['token'] = order.token
    else:
        if 'token' in request.session:
            try:
                if Order.objects.filter(user=None, token=request.session['token'], archive=False) is not None:
                    order = Order.objects.filter(user=None, token=request.session['token'], archive=False).latest('create_time')
                else:
                    order = Order.objects.create(user=None)
                    request.session['token'] = order.token
            except Order.DoesNotExist:
                order = Order.objects.create(user=None)
                request.session['token'] = order.token
        else:
            order = Order.objects.create(user=None)
            request.session['token'] = order.token
    return order


def main(request):
    if request.user.is_authenticated():
        messages = get_actual_messages(request)
        return render(request, 'user/main.html', {'messages': messages})
    return render(request, 'user/main.html')


def login_user(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        print(form.is_valid())
        username = form.instance.username
        password = form.instance.password

        try:
            user = User.objects.get(username=username)
            if user is not None and password == user.password:
                user.backend = 'django.contrib.auth.backends.ModelBackend'
                login(request, user)
                return redirect('/')
        except User.DoesNotExist:
            form = LoginForm()
            context = {'form': form}
            if request.user.is_authenticated():
                messages = get_actual_messages(request)
                context = {'form': form, 'messages': messages}
            return render(request, 'user/login.html', context)
    else:
        form = LoginForm()

    context = {'form': form}
    if request.user.is_authenticated():
        messages = get_actual_messages(request)
        context = {'form': form, 'messages': messages}
    return render(request, 'user/login.html', context)


def register_user(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.instance.username
            form.instance.first_name = username
            try:
                if User.objects.get(username=username) is not None:
                    return render(request, 'user/register.html', {'form': form})
            except User.DoesNotExist:
                pass
            user = form.save()
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
            return redirect('/')
        else:
            form = RegisterForm()
    else:
        form = RegisterForm()

    context = {'form': form}
    if request.user.is_authenticated():
        messages = get_actual_messages(request)
        context = {'form': form, 'messages': messages}
    return render(request, 'user/register.html', context)


def construct(request):
    if request.method == 'POST':
        form = ImageForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            form.instance.author = request.user
            form.instance.cost = form.instance.extra_cost + 500
            form.save()
            return redirect('/user/construct-success')
        else:
            form = ImageForm()
    else:
        form = ImageForm()

    context = {'form': form}
    if request.user.is_authenticated():
        messages = get_actual_messages(request)
        context = {'form': form, 'messages': messages}
    return render(request, 'user/construct.html', context)


def construct_success(request):
    if request.user.is_authenticated():
        messages = get_actual_messages(request)
        return render(request, 'user/construct_success.html', {'messages': messages})
    return render(request, 'user/construct_success.html')


def catalog(request):
    cache.get_or_set('catalog', Image.objects.filter(accepted=True), 60)
    images = cache.get('catalog')
    context = {'images': images}
    if request.user.is_authenticated():
        messages = get_actual_messages(request)
        context = {'images': images, 'messages': messages}
    return render(request, 'user/catalog.html', context)


def shipping(request):
    return render(request, 'user/shipping.html')


def preview(request):
    if request.method == 'POST':
        image_id = int(request.POST.get('catalog_input'))
        image = Image.objects.get(id=image_id)
        context = {'image': image}
        if request.user.is_authenticated():
            messages = get_actual_messages(request)
            context = {'image': image, 'messages': messages}
        return render(request, 'user/preview.html', context)

    if request.user.is_authenticated():
        messages = get_actual_messages(request)
        return render(request, 'user/preview.html', {'messages': messages})
    return render(request, 'user/preview.html')


def add(request):
    if request.method == 'GET' and request.is_ajax():
        size = request.GET.get('size')
        color = request.GET.get('color')
        quantity = int(request.GET.get('quantity'))
        image_id = int(request.GET.get('image_id'))
        image = Image.objects.get(id=image_id)
        cost = int(quantity)*image.cost
        new_item = Item.objects.create(image=image, size=size, color=color, cost=cost, quantity=quantity)
        order = get_or_create_order(request)
        order.item.add(new_item)
        order.total_cost = order.total_cost + new_item.cost
        order.total_quantity = order.total_quantity + quantity
        order.save()
        Wages.objects.create(user=image.author, money=cost, order=order)
    return HttpResponse(u'Предмет добавлен')


def save_options(request):
    if request.method == 'GET' and request.is_ajax():
        region = request.GET.get('region')
        address = request.GET.get('address')
        order = get_or_create_order(request)
        order.shipping_address = address
        order.shipping_region = region
        order.save()
    return HttpResponse()


def delete_item(request):
    if request.method == 'POST' and request.is_ajax():
        id = int(request.POST.get('id'))
        order = Order.objects.filter(user=request.user, archive=False).latest('create_time')
        item = Item.objects.get(id=id)
        if order.item.all().get(id=id) is not None:
            order.item.remove(item)
            order.total_cost = order.total_cost - item.cost
            order.total_quantity = order.total_quantity - item.quantity
            item.delete()
            order.save()
            image = item.image
            wages = Wages.objects.filter(user=image.author, order=order)
            wages.delete()
        return JsonResponse({'new_total_cost': order.total_cost, 'new_total_quantity': order.total_quantity})
    return None

def order(request):
    order = get_or_create_order(request)
    context = {'order': order}
    if request.user.is_authenticated():
        messages = get_actual_messages(request)
        context = {'order': order, 'messages': messages}
    return render(request, 'user/order.html', context)


@login_required
def account_profile(request):
    return redirect('/')


def account_logout(request):
    logout(request)
    return redirect('/')


@csrf_exempt
def paypal_success(request):
    if request.user.is_authenticated():
        messages = get_actual_messages(request)
        order = get_or_create_order(request)
        order.archive = True
        order.save()
        payment = Payment.objects.create(order=order)
        payment.checkout_time = timezone.now()
        payment.save()
        wages = Wages.objects.filter(order=order)
        for wage in wages:
            wage.valid = True
            wage.save()
        return render(request, 'user/payment_success.html', {'messages': messages})
    return render(request, 'user/payment_success.html')


@login_required
def paypal_pay(request):
    order = Order.objects.filter(user=request.user, archive=False).latest('create_time')
    paypal_dict = {
        "business": "bobbythehiver-facilitator@gmail.com",
        "amount": order.total_cost,
        "currency_code": "RUB",
        "item_name": u"Заказ №" + str(order.id),
        "invoice": "INV-" + str(order.id),
        "notify_url": reverse('paypal-ipn'),
        "return_url": "http://localhost:8000/user/payment/success/",
        "cancel_return": "http://localhost:8000/user/payment/cart/",
        "custom": str(request.user.id)
    }

    form = PayPalPaymentsForm(initial=paypal_dict)
    context = {'form': form, 'paypal_dict': paypal_dict}
    if request.user.is_authenticated():
        messages = get_actual_messages(request)
        context = {'form': form, 'paypal_dict': paypal_dict, 'messages': messages}
    return render(request, 'user/payment.html', context)

