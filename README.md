Анна Воронцова

Сайт, принимающий заказы на печать футболок.
Можно выбрать товары из каталога или создать свой предмет в конструкторе, загрузив изображение со своего компьютера. Каталог пополняется работами пользователей. Цена на товары складывается из базовой стоимость печати и наценки, установленной автором работы. Оплата производится через электронную платежную систему, выручка от продаж товаров поступает на личный счет автора на сайте.

Как это будет происходить:
см. файл EStore.pdf

Как это будет выглядеть:

![0_mainpage1.png](https://bitbucket.org/repo/9GgGRE/images/1599389617-0_mainpage1.png)
![1_diy.png](https://bitbucket.org/repo/9GgGRE/images/2157343924-1_diy.png)
![2_gallery.png](https://bitbucket.org/repo/9GgGRE/images/599472052-2_gallery.png)
![3_order.png](https://bitbucket.org/repo/9GgGRE/images/372473601-3_order.png)
![4_order2.png](https://bitbucket.org/repo/9GgGRE/images/3736328264-4_order2.png)
![5_auth-reg.png](https://bitbucket.org/repo/9GgGRE/images/2026202600-5_auth-reg.png)